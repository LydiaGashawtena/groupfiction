﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fiction2.Data;
using Fiction2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Fiction2.Controllers
{
    public class MainController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> userManager;

        public MainController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => userManager.GetUserAsync(HttpContext.User);

        public IList<Book> Books { get; set; }
        public IList<Book> MyBooks { get; set; }
        public IList<Group> Groups { get; set; }
        public IList<Group> MyGroups { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public async Task<IActionResult> Main(string SearchString)
        {

            var user = await GetCurrentUserAsync();
            IQueryable<Book> books = db.Books;
            if (!string.IsNullOrEmpty(SearchString))
            {
                books = books.Where(b => b.Name.Contains(SearchString));
            }
            Books = await books.ToListAsync();
            MyBooks = new List<Book>();
            MyGroups = await db.Groups.Where(emp => emp.GroupMembers.Any(r => r.ApplicationUserId == user.Id)).Include(g => g.Books).ToListAsync();
            foreach (Group group in MyGroups)
            {
                foreach (Book book in group.Books)
                {
                    MyBooks.Add(book);
                }
            }
            MainViewModel mainViewModel = new MainViewModel() { Books = Books, MyBooks = MyBooks, MyGroups = MyGroups };
            return View(mainViewModel);
        }

        [HttpPost]
        public string Main(string searchString, bool notUsed)
        {
            return "From [HttpPost]Main: filter on " + searchString;
        }

        public async Task<IActionResult> BrowseGroups(string SearchString)
        {
            var user = await GetCurrentUserAsync();
            IQueryable<Group> groups =  db.Groups.Include(c => c.GroupMembers).ThenInclude(x => x.ApplicationUser); 
            if (!string.IsNullOrEmpty(SearchString))
            {
                groups = groups.Where(b => b.Name.Contains(SearchString));
            }
            Groups = await groups.ToListAsync();
            
            MyBooks = new List<Book>();
            MyGroups = await db.Groups.Where(emp => emp.GroupMembers.Any(r => r.ApplicationUserId == user.Id)).Include(g => g.Books).ToListAsync();
            foreach (Group group in MyGroups)
            {
                foreach (Book book in group.Books)
                {
                    MyBooks.Add(book);
                }
            }

            MainViewModel mainViewModel = new MainViewModel() { Groups = Groups, MyBooks = MyBooks, MyGroups = MyGroups };
            return View(mainViewModel);
        }

        [HttpPost]
        public string BrowseGroups(string searchString, bool notUsed)
        {
            return "From [HttpPost]BrowseGroups: filter on " + searchString;
        }

        public async Task<IActionResult> Profile(string id)
        {
            IList<ApplicationUser> users = await userManager.Users.Where(u => u.Id == id).Include(u => u.GroupMembers).ToListAsync();
            var user = users[0];
            IList<Book> MyBooks = new List<Book>();
            IList<Group> MyGroups = await db.Groups.Where(emp => emp.GroupMembers.Any(r => r.ApplicationUserId == user.Id)).Include(g => g.Books).ToListAsync();
            foreach (Group group in MyGroups)
            {
                foreach (Book book in group.Books)
                {
                    MyBooks.Add(book);
                }
            }
            UserViewModel userViewModel = new UserViewModel() { ApplicationUser = user, Books = MyBooks};
            return View(userViewModel);
        }
    }
}