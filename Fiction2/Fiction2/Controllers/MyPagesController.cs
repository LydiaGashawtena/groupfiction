﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fiction2.Data;
using Fiction2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Fiction2.Controllers
{
    public class MyPagesController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> userManager;

        public MyPagesController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => userManager.GetUserAsync(HttpContext.User);

        public IList<Book> Books { get; set; }
        public IList<Group> Groups { get; set; }

        public async Task<IActionResult> MyBooks()
        {
            Books = new List<Book>();
            var user = await GetCurrentUserAsync();
            Groups = await db.Groups.Where(emp => emp.GroupMembers.Any(r => r.ApplicationUserId == user.Id)).Include(g => g.Books).ToListAsync();
            foreach (Group group in Groups)
            {
                foreach (Book book in group.Books)
                {
                    Books.Add(book);
                }
            }

            MainViewModel mainViewModel = new MainViewModel() { MyBooks = Books, MyGroups = Groups};
            return View(mainViewModel);
        }

        public async Task<IActionResult> MyGroups()
        {
            var user = await GetCurrentUserAsync();
            Groups = await db.Groups.Where(emp => emp.GroupMembers.Any(r => r.ApplicationUserId == user.Id)).Include(c => c.GroupMembers).ThenInclude(x => x.ApplicationUser).ToListAsync();
            return View(Groups);
        }

        [HttpPost]
        public async Task<IActionResult> CreateGroup(Group group)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                var user = await GetCurrentUserAsync();
                //Group group = new Group { Name = Input.Name, Description = Input.Description };
                GroupMember groupMember = new GroupMember { ApplicationUser = user, Group = group };
                db.Groups.Add(group);
                db.GroupMembers.Add(groupMember);
                await db.SaveChangesAsync();
                return RedirectToAction("MyGroups");
            }

        }

        [HttpPost]
        public async Task<IActionResult> CreateBook(CreateBookViewModel bookModel, IFormFile Image)
        {
            byte[] temp = null;
            
                if (Image.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await Image.CopyToAsync(stream);
                        temp = stream.ToArray();
                    }
                }
            

            IList<Group> groups = await db.Groups.Where(g => g.Id == bookModel.GroupId).ToListAsync();
            DateTime date = DateTime.Now;
            var user = await GetCurrentUserAsync();
            string updatedBy = user.UserName;
            Book book = new Book { Name = bookModel.Name, Description = bookModel.Description, Image = temp, Group = groups[0], LastUpdated = date, LastUpdatedBy = updatedBy };
            db.Books.Add(book);
            await db.SaveChangesAsync();
            return RedirectToAction("MyBooks");
        }

        private bool isUsers;
        public Group Group { get; set; }

        public async Task<IActionResult> ViewGroup(int id)
        {
            isUsers = true;
            var user = await GetCurrentUserAsync();
            Group = await db.Groups.Include(c => c.GroupMembers).ThenInclude(x => x.ApplicationUser).Include(c => c.JoinRequests).ThenInclude(x => x.ApplicationUser).SingleOrDefaultAsync(i => i.Id == id);
            Books = await db.Books.Where(b => b.Group.Id == Group.Id).ToListAsync();
            int count = 0;
            foreach (var item in Group.GroupMembers)
            {
                if (item.ApplicationUserId == user.Id)
                {
                    count++;
                }
            }
            if (count > 0)
            {
                isUsers = true;
            }
            else
            {
                isUsers = false;
            }
            GroupViewModel groupViewModel = new GroupViewModel() { Id = Group.Id, Name = Group.Name, Description = Group.Description, Books = Books, GroupMembers = Group.GroupMembers, JoinRequests = Group.JoinRequests, IsUsers = isUsers };
            return View(groupViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> JoinGroup(int Id)
        {
            var user = await GetCurrentUserAsync();
            Group Group = await db.Groups.SingleOrDefaultAsync(i => i.Id == Id);
            JoinRequest joinRequest = new JoinRequest { ApplicationUser = user, Group = Group };
            db.JoinRequests.Add(joinRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("BrowseGroups", "Main");
        }

        [HttpPost]
        public async Task<IActionResult> AcceptRequest(int Id, string UserId)
        {
            IList<ApplicationUser> users = await userManager.Users.Where(u => u.Id == UserId).ToListAsync();
            var user = users[0];
            Group Group = await db.Groups.SingleOrDefaultAsync(i => i.Id == Id);
            GroupMember groupMember = new GroupMember { ApplicationUser = user, Group = Group };
            JoinRequest joinRequest = await db.JoinRequests.FindAsync(UserId, Id);
            db.GroupMembers.Add(groupMember);
            db.JoinRequests.Remove(joinRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("ViewGroup", new { id = Id });
        }

        [HttpPost]
        public async Task<IActionResult> DeclineRequest(int Id, string UserId)
        {
            IList<ApplicationUser> users = await userManager.Users.Where(u => u.Id == UserId).ToListAsync();
            var user = users[0];
            Group Group = await db.Groups.SingleOrDefaultAsync(i => i.Id == Id);
            JoinRequest joinRequest = await db.JoinRequests.FindAsync(UserId, Id);
            db.JoinRequests.Remove(joinRequest);
            await db.SaveChangesAsync();
            return RedirectToAction("ViewGroup", new { id = Id });
        }

    }
}