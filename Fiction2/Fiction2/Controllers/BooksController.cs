﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fiction2.Data;
using Fiction2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Fiction2.Controllers
{
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext db;
        private readonly UserManager<ApplicationUser> userManager;

        public BooksController(ApplicationDbContext db, UserManager<ApplicationUser> userManager)
        {
            this.db = db;
            this.userManager = userManager;
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => userManager.GetUserAsync(HttpContext.User);

        public Book Book { get; set; }
        public IList<Post> Posts { get; set; }

        public async Task<IActionResult> Write(int id)
        {
            Book = await db.Books.Include(b => b.Group).Include(b => b.Posts).ThenInclude(p => p.ApplicationUser).SingleOrDefaultAsync(i => i.Id == id);
            Posts = Book.Posts.OrderByDescending(s => s.Id).ToList();
            BookViewModel bookViewModel = new BookViewModel() { Book = Book, Posts = Posts };
            return View(bookViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Save(BookViewModel bookModel)
        {
            bookModel.Book.LastUpdated = DateTime.Now;
            var user = await GetCurrentUserAsync();
            bookModel.Book.LastUpdatedBy = user.UserName;
            db.Attach(bookModel.Book).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw new Exception("Book not found");
            }
            return RedirectToAction("Write", new { id = bookModel.Book.Id});
        }

        [HttpPost]
        public async Task<IActionResult> Post(BookViewModel bookModel, int BookId)
        {
            var user = await GetCurrentUserAsync();
            Book Book = await db.Books.SingleOrDefaultAsync(i => i.Id == BookId);
            bookModel.Post.ApplicationUser = user;
            bookModel.Post.Date = DateTime.Now;
            bookModel.Post.Book = Book;
            
            db.Posts.Add(bookModel.Post);
            await db.SaveChangesAsync();
            return RedirectToAction("Write", new { id = BookId });
        }

        public async Task<IActionResult> Read(int id)
        {
            Book = await db.Books.Include(c => c.Group).ThenInclude(x => x.GroupMembers).ThenInclude(g => g.ApplicationUser).SingleOrDefaultAsync(i => i.Id == id);
            return View(Book);
        }
    }
}