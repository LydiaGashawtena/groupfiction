﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class GroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public ICollection<GroupMember> GroupMembers { get; set; }
        public ICollection<JoinRequest> JoinRequests { get; set; }
        public ICollection<Book> Books { get; set; }
        public bool IsUsers { get; set; }
    }
}
