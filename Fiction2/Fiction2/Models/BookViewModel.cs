﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class BookViewModel
    {
        public Book Book { get; set; }
        public IList<Post> Posts { get; set; }
        public Post Post { get; set; }
        public int BookId { get; set; }
    }
}
