﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class CreateBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public int GroupId { get; set; }
        public IList<Group> MyGroups { get; set; }

    }
}
