﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class MainViewModel
    {
        public IList<Book> Books { get; set; }
        public IList<Book> MyBooks { get; set; }
        public IList<Group> Groups { get; set; }
        public IList<Group> MyGroups { get; set; }
      
    }
}
