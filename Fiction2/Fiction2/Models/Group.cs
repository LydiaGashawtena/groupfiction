﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class Group
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public ICollection<GroupMember> GroupMembers { get; set; }
        public ICollection<JoinRequest> JoinRequests { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
