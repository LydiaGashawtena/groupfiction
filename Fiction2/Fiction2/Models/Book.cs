﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public String Description { get; set; }
        public string Content { get; set; }
        public byte[] Image { get; set; }
        public Group Group { get; set; }
        public DateTime LastUpdated { get; set; }
        public string LastUpdatedBy { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
