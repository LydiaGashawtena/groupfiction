﻿using Fiction2.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Models
{
    public class UserViewModel
    {
        public ApplicationUser ApplicationUser { get; set; }
        public IList<Book> Books { get; set; }
    }
}
