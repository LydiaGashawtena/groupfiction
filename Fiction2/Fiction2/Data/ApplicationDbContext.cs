﻿using System;
using System.Collections.Generic;
using System.Text;
using Fiction2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Fiction2.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<JoinRequest> JoinRequests { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GroupMember>()
                .HasKey(g => new { g.ApplicationUserId, g.GroupId });

            modelBuilder.Entity<JoinRequest>()
                .HasKey(g => new { g.ApplicationUserId, g.GroupId });

            modelBuilder.Entity<Group>()
                .HasMany(g => g.Books)
                .WithOne(b => b.Group);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(g => g.Posts)
                .WithOne(b => b.ApplicationUser);

            modelBuilder.Entity<Book>()
               .HasMany(g => g.Posts)
               .WithOne(b => b.Book);
        }
    }
}
