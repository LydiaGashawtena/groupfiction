﻿using Fiction2.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fiction2.Data
{
    public class ApplicationUser : IdentityUser
    {
        public String FullName { get; set; }
        public byte[] Image { get; set; }
        public String About { get; set; }
        public ICollection<GroupMember> GroupMembers { get; set; }
        public ICollection<JoinRequest> JoinRequests { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
